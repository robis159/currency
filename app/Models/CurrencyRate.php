<?php

namespace Currency\Models;

use Illuminate\Database\Eloquent\Model;
class CurrencyRate extends Model
{
    protected $fillable = [
        'currency_id', 'rate',
    ];

    public function curency() {
        return $this->belongsTo('Currency\Models\Currency');
    }
}
