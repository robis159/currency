<?php

namespace Currency\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'code', 'country',
    ];

    public function rate() {
        return $this->hasOne('Currency\Models\CurrencyRate', 'currency_id', 'id');
    }
}
