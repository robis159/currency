<?php

namespace Currency\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyType extends Model
{
    protected $fillable = [
        'code'
    ];
}
