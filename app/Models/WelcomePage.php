<?php

namespace Currency\Models;

use Illuminate\Database\Eloquent\Model;

class WelcomePage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'greeting_text',
        'title1',
        'title2',
        'title3',
        'title4',
        'text_area1',
        'text_area2',
        'text_area3',
        'text_area4',
    ];

    public $timestamps = false;
}
