<?php

namespace Currency\Http\Controllers;

use Illuminate\Http\Request;

use Currency\Models\WelcomePage;

class WelcomePageController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     *
     */
     public function index()
     {
        $data = WelcomePage::all()->first();

        if(empty($data)) {
            $data = 0;
        }

        return view('welcome')->with('data', $data);
     }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Store(Request $request)
    {
        WelcomePage::create([
            'greeting_text' => $request->greetings,
            'title1' => $request->title1,
            'title2' => $request->title2,
            'title3' => $request->title3,
            'title4' => $request->title4,
            'text_area1' => $request->title1_text,
            'text_area2' => $request->title2_text,
            'text_area3' => $request->title3_text,
            'text_area4' => $request->title4_text,
        ]);

        return redirect()->route('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
        WelcomePage::where('id', $id)->update([
            'greeting_text' => $request->greetings,
            'title1' => $request->title1,
            'title2' => $request->title2,
            'title3' => $request->title3,
            'title4' => $request->title4,
            'text_area1' => $request->title1_text,
            'text_area2' => $request->title2_text,
            'text_area3' => $request->title3_text,
            'text_area4' => $request->title4_text,
        ]);

        return redirect()->route('home');
     }
}
