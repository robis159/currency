<?php

namespace Currency\Http\Controllers;

use Illuminate\Http\Request;

use Currency\Models\CurrencyRate;
use Currency\Models\WelcomePage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rates = CurrencyRate::all();
        $data = WelcomePage::all()->first();

         if(empty($data)) {
            $data = 0;
         }

         return view('home', [
            'rates' => $rates,
            'welcome' => $data
        ]);
    }

}
