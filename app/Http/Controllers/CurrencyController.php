<?php

namespace Currency\Http\Controllers;

use Illuminate\Http\Request;

use Currency\Models\Currency;
use Currency\Models\CurrencyRate;
use Currency\Models\CurrencyType;

use GuzzleHttp\Client;

class CurrencyController extends Controller
{
    const API_KEY = '5c290985e8401';
    const API_SECRET = 'cc02527c530f7c242e02d594dcbbc145';
    const BASE_URL = 'https://bankersalgo.com/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * show all currencies list in table
     *
     * @return void
     */
    public function index() {
        $data = array();
        $codes = array();
        $cur = Currency::all();
        $check = CurrencyRate::all();

        if ($check->isNotEmpty()) {
            $route = 'rate-update';
        } else {
            $route = 'rate-store';
        }

        if ($cur->isNotEmpty()) {
            $type = '';
            if ($check->isNotEmpty()) {
                $type = CurrencyType::all()->first()->code;
            }

                foreach ($cur as $c) {
                    $codes += [$c->code => $c->code];
                    $rate = CurrencyRate::where('currency_id', $c->id)->first();
                    if ($rate) {
                        if($rate->rate == 0) {
                            continue;
                        }
                        array_push($data, [
                            'country' => $c->country,
                            'code' => $c->code,
                            'rate' => $rate->rate,
                        ]);
                    }
                }
        } else {
            $type = '';
        }

        return view('currencyList', [
            'data' => $data,
            'codes' => $codes,
            'route' => $route,
            'type' => $type
        ]);
    }
    /**
     * store currencies country name and code in database table currencies
     *
     * @return void
     */
    public function currenciesStore() {

        $data = $this->getCurrencies();

        foreach ($data->currencies as $key => $value) {
            if (strlen($key) > 4) {
                continue;
            }
            Currency::create([
                'code' => $key,
                'country' => $value
            ]);
        }
        return redirect()->route('currency-index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function rateStore(Request $request)
     {
        $data = $this->getCurrencyRates($request->type);

        foreach ($data->rates as $key => $value) {
           $cur = Currency::where('code', $key)->first();
           if($cur !== null) {
            $cur->rate()->create([
                    'rate' => $value
            ]);
           }
        }

        CurrencyType::create([
            'code' => $request->type
        ]);
        return redirect()->route('currency-index');
     }

     /**
     * Update rates
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function rateUpdate(Request $request)
     {

        $data = $this->getCurrencyRates($request->type);

        foreach ($data->rates as $key => $value) {
           $cur = Currency::where('code', $key)->first();
           if($cur !== null) {
            $cur->rate()->update([
                    'rate' => $value
            ]);
           }
        }
        $oldType = CurrencyType::all()->first();
        CurrencyType::where('code', $oldType->code)->update([
            'code' => $request->type
        ]);
        return redirect()->route('currency-index');
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function calculate(Request $request)
     {
        $codes = array();
        $cur = Currency::all();

        foreach ($cur as $c) {
            $codes += [$c->code => $c->code];
        }

        $from = $request->from_type;
        $to = $request->to_type;
        $amount = $request->amount;

        $result = $this->calculateCurrencyRates($from, $to, $amount);
        return view('rateCalculator', [
            'codes' => $codes,
            'from' =>$from,
            'to' =>$to,
            'amount' =>$amount,
            'result' =>$result,
        ]);
     }
     /**
     * calculate curencies
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function calculateIndex(Request $request)
     {
        $codes = array();
        $cur = Currency::all();

        foreach ($cur as $c) {
            $codes += [$c->code => $c->code];
        }

        $from = 'EUR';
        $to = 'USD';
        $amount = 1;

        $result = $this->calculateCurrencyRates($from, $to, $amount);
        return view('rateCalculator', [
            'codes' => $codes,
            'from' =>$from,
            'to' =>$to,
            'amount' =>$amount,
            'result' =>$result,
        ]);
     }

    /**
     * Get currenciec country name and code
     *
     * @return void
     */
    protected function getCurrencies() {
        $url = self::BASE_URL . 'currencies_names_json';

        $client = new Client();
        $currencies = $client->request('GET', $url);

        return json_decode($currencies->getBody()->getContents());
    }

    /**
     * get currency rates using base currency
     *
     * @param string $base - currency code
     * @return void
     */
    protected function getCurrencyRates($base) {
        $end_pont = self::BASE_URL . 'apirates2/';

        $url = $end_pont . self::API_KEY . '/' . self::API_SECRET . '/' . $base;

        $client = new client();
        $rate = $client->request('GET', $url);

        return json_decode($rate->getBody()->getContents());
    }

    /**
     * calculate of selected currencies
     *
     * @param string $base - currency code
     * @return void
     */
     protected function calculateCurrencyRates($from, $to, $amount) {
        $end_pont = self::BASE_URL . 'apicalc2/';

        $url = $end_pont . self::API_KEY . '/' . self::API_SECRET . '/' . $from . '/' . $to . '/' . $amount;

        $client = new client();
        $rate = $client->request('GET', $url);

        return json_decode($rate->getBody()->getContents());
    }
}


