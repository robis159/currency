<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWelcomePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('welcome_pages', function ($table) {
            $table->increments('id');
            $table->string('greeting_text', 100)->nullable();
            $table->string('title1', 100)->nullable();
            $table->string('title2', 100)->nullable();
            $table->string('title3', 100)->nullable();
            $table->string('title4', 100)->nullable();
            $table->text('text_area1')->nullable();
            $table->text('text_area2')->nullable();
            $table->text('text_area3')->nullable();
            $table->text('text_area4')->nullable();
        });
        // Insert some stuff
        DB::table('welcome_pages')->insert(
            array(
                'greeting_text' => 'Welcome to Currency Converter App',
                'title1' => 'About currency',
                'text_area1' => 'A currency (from Middle English: curraunt, "in circulation", from Latin: currens, -entis), in the most specific use of the word, refers to money in any form when in actual use or circulation as a medium of exchange, especially circulating banknotes and coins.[1][2] A more general definition is that a currency is a system of money (monetary units) in common use, especially in a nation.[3] Under this definition, US dollars, British pounds, Australian dollars, European euros and Russian ruble are examples of currency. These various currencies are recognized as stores of value and are traded between nations in foreign exchange markets, which determine the relative values of the different currencies.[4] Currencies in this sense are defined by governments, and each type has limited boundaries of acceptance.',
                'text_area2' => 'Other definitions of the term "currency" are discussed in their respective synonymous articles banknote, coin, and money. The latter definition, pertaining to the currency systems of nations, is the topic of this article. Currencies can be classified into two monetary systems: fiat money and commodity money, depending on what guarantees the value (the economy at large vs. the government\'s physical metal reserves). Some currencies are legal tender in certain political jurisdictions. Others are simply traded for their economic value. Digital currency has arisen with the popularity of computers and the Internet',
            ));
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('welcome_pages');
    }
}
