
@extends('layouts.app')

@section('content')
    {{-- @if ($data === 0)
    @else --}}
        {{-- greatings text --}}
        @if ($data->greeting_text !== null)
             <h1>{!! $data->greeting_text !!}</h1>
        @endif

        {{-- text area 1 --}}
        @if ($data->title1 !== null)
            <h3>{!! $data->title1 !!}</h3>
        @endif
        @if ($data->text_area1 !== null)
            <p>{!! $data->text_area1 !!}</p>
        @endif

        {{-- text area 2 --}}
        @if ($data->title2 !== null)
            <h3>{!! $data->title2 !!}</h3>
        @endif
        @if ($data->text_area2 !== null)
            <p>{!! $data->text_area2 !!}</p>
        @endif

        {{-- text area 3 --}}
        @if ($data->title3 !== null)
            <h1>{!! $data->title3 !!}</h1>
        @endif
        @if ($data->text_area3 !== null)
            <p>{!! $data->text_area3 !!}</p>
        @endif

        {{-- text area 4 --}}
        @if ($data->title4 !== null)
            <h1>{!! $data->title4 !!}</h1>
        @endif
        @if ($data->text_area4 !== null)
            <p>{!! $data->text_area4 !!}</p>
        @endif

    {{-- @endif --}}
@endsection