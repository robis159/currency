<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta accept-encoding="gzip">
    <meta accept-encoding="deflate">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="container">
        {{-- include navigation bar --}}
        @include('include.navBar')

        <main class="py-4">
            @yield('content')
        </main>
    </div>

<script>
    // CKEDITOR.editorConfig = function( config ) {
    //     // misc options
    //     config.height = '50px';
    // };
    CKEDITOR.replace( 'editor1',);
    CKEDITOR.replace( 'editor2',);
    CKEDITOR.replace( 'editor3',);
    CKEDITOR.replace( 'editor4',);
    CKEDITOR.replace( 'editor5',);
    CKEDITOR.replace( 'editor6',);
    CKEDITOR.replace( 'editor7',);
    CKEDITOR.replace( 'editor8',);
    CKEDITOR.replace( 'editor9',);
</script>
</body>
</html>
