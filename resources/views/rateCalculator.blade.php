@extends('layouts.app')

@section('content')
<style>
.size {
    width: 100px;
}

</style>
    <h1>Currency calculator</h1>
    <hr>

    {{ Form::open(['route' => 'calculator-cal', 'method' => 'POST']) }}
        <div class="row form-group">

            {{ Form::label('amount', 'amount:' , ['class' => 'ml-3 h4 mt-2 mr-1']) }}
            {{ Form::number('amount', floatval($amount), ['class' => ' size input-group-text']) }}

            {{ Form::label('from_type', 'From:', ['class' => 'ml-3 h4 mt-2 mr-1']) }}
            {{ Form::select('from_type', $codes, $from,  ['class' => 'form-control size']) }}

            {{ Form::label('to_type', 'to:' , ['class' => 'ml-3 h4 mt-2 mr-1']) }}
            {{ Form::select('to_type', $codes, $to,  ['class' => 'form-control size']) }}

            {{ Form::submit('Result', ['class' => 'btn btn-primary ml-3']) }}
        </div>
        <div class="row">
            {{ Form::label('number', 'Result = '.sprintf("%.2f", $result->result) . ' ' . $to, ['class' => 'h3 mt-3 ml-3']) }}
        </div>
    {{ Form::close() }}

@endsection