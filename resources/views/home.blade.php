@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (sizeof($rates) === 0)
                        <h4>The database is blank, <br> let's add the data by pressing the buttons below</h4>

                        <div class="mb-2">
                            <a href="{{ route('currencies-store') }}" class="btn btn-secondary">Get currencies names</a>
                        </div>
                        <hr>
                    @endif

                    <h4>Change home page information</h4>
                    <hr>
                        @if ($welcome !== 0)
                            @include('updateWellcomePage')
                        @else
                            @include('createWellcomePage')
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
