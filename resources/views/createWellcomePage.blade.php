{{ Form::open(['route' => 'welcome-store', 'method' => 'POST']) }}
    {{-- Greating --}}
        <div class="col-lg-4 ">
            {!! Form::label('greetings', 'Greetings text', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {{ Form::textarea('greetings', '', ['id' => 'editor1', 'class' => 'textarea-title']) }}
        </div>
    <hr>

    {{-- text area 1 --}}
        <div class="col-lg-4 ">
            {!! Form::label('title1', 'Text area 1 - title', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title1', '', ['id' => 'editor2', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

        <div class="col-lg-4 ">
            {!! Form::label('title1_text', 'Text area 1 - Text', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title1_text', '', ['id' => 'editor3', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

    {{-- text area 2 --}}
        <div class="col-lg-4 ">
            {!! Form::label('title2', 'Text area 2 - title', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title2', '', ['id' => 'editor4', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

        <div class="col-lg-4 ">
            {!! Form::label('title2_text', 'Text area 2 - Text', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title2_text', '', ['id' => 'editor5', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

    {{-- text area 3 --}}
        <div class="col-lg-4 ">
            {!! Form::label('title3', 'Text area 3 - title', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title3', '', ['id' => 'editor6', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

        <div class="col-lg-4 ">
            {!! Form::label('title3_text', 'Text area 3 - Text', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title3_text', '', ['id' => 'editor7', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

    {{-- text area 4 --}}
        <div class="col-lg-4 ">
            {!! Form::label('title4', 'Text area 4 - title', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title4', '', ['id' => 'editor8', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

        <div class="col-lg-4 ">
            {!! Form::label('title4_text', 'Text area 4 - Text', ['class' => 'h4 ml-3']) !!}
        </div>
        <div class="col-lg-12 ">
            {!! Form::textarea('title4_text', '', ['id' => 'editor9', 'class' => 'textarea-title']) !!}
        </div>
    <hr>

    <div class="row">
        {!! Form::submit('save', ['class' => 'btn btn-primary w-50 mx-auto mb-2']) !!}
    </div>
{!! Form::open() !!}