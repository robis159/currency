@extends('layouts.app')

@section('content')
<style>
.size {
    width: 100px;
}
</style>

<div class="mb-2">
    <h1>Currency rate list</h1>
    <hr>
    @if (!empty($codes))
        {{ Form::open(['route' => $route, 'method' => 'POST']) }}
            <div class="row">
                    {{ Form::select('type', $codes, $type,  ['class' => 'form-control ml-3 size']) }}
                    {{ Form::submit('Select', ['route' => 'rate-store', 'class' => 'btn btn->primary ml-2']) }}
            </div>
        {{ Form::close() }}
    @endif
</div>

<table class="table">
    <thead class="thead-dark">
        <tr>
        <th scope="col">#</th>
        <th scope="col">Country</th>
        <th scope="col">Code</th>
        <th scope="col">Rate</th>
        </tr>
    </thead>
    <tbody>
        @php ($number = 1)
        @foreach ($data as $d)
        <tr>
            <th scope="row">{{ $number }}</th>
            <td>{{ $d['country'] }}</td>
            <td>{{ $d['code'] }}</td>
            <td>{{ $d['rate'] }}</td>
        </tr>
        @php($number++)
        @endforeach
    </tbody>
</table>

@endsection