<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomePageController@index');
Route::post('/welcome-page-store', 'WelcomePageController@store')->name('welcome-store');
Route::post('/welcome-page-update/{id}', 'WelcomePageController@update')->name('welcome-update');

Route::get('/currency-store', 'CurrencyController@currenciesStore')->name('currencies-store');
Route::post('/rate-store', 'CurrencyController@rateStore')->name('rate-store');
Route::post('/rate-update', 'CurrencyController@rateUpdate')->name('rate-update');
Route::get('/currency-index', 'CurrencyController@index')->name('currency-index');
Route::get('/calculator-index', 'CurrencyController@calculateIndex')->name('calculator-index');
Route::post('/calculator-calculate', 'CurrencyController@calculate')->name('calculator-cal');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


